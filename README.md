# WatchWithPeers
## WwP

## About the Project
Watch videos with your peers together online mainly focused on [PeerTube](https://joinpeertube.org/) a great
open source video sharing platform. I felt the need for a platform where we can watch peertube videos with our 
peers in realtime.

#### Existing platforms:
* [Watch2gether](https://w2g.tv/) (proprietary) (website)
* [SyncWatch](https://github.com/Semro/syncwatch) (Open source) (Browser extension)

#### Existing Github Repositories:

* [Watch2gether Clone project in Github](https://github.com/Virtual1/watch2gether-clone) Selected!!

After researching about the existing platforms and repos I ended up cloning the above mentioned repo.


### Built With
List of major frameworks used within this project:
* [Node.js](https://nodejs.org)
* [Express](https://expressjs.com)
* [Socket.io](https://socket.io)
* [Mongoose](https://mongoosejs.com/)
* [Bootstrap](https://getbootstrap.com)


## Getting Started
To get a local copy up and running follow these simple steps.

### Prerequisites
* Node.js installed (<a href="https://nodejs.org/en/download">Download here</a>)
* MongoDB installed & started (<a href="https://www.mongodb.com/try/download/community">Download here</a>)

### Installation
1. Clone the repo
   ```sh
   git clone https://gitlab.com/atulyaraj/watchwithpeers.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```
3. Create ".env" file in root dir with contents:
    ```
    PORT=3000
    DOMAIN=http://localhost:3000
    GOOGLE_YOUTUBE_API_KEY=GOOGLE-YOUTUBE-API-KEY
    MONGO_URI=
    MONGO_USER=
    MONGO_USER_PASS=
    ```

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request


<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.



## Inside the Project:

## #TODO

* Create a logo for project






```
Open the site -> Create room -> Name of room -> Enter room
                             |     
              -> Join room___|
```

### Inside room:
* Insert link section
* Video player section
* Users section
    * Users in room
    * Logout option
* Chat section


### Platforms:
* Web application, so any browser will do with any platform.

```
Client <---> Server <---> Client
```


### Implementation:

1. Create server
2. Create client
3. Connect client to server
4. Create rooms
5. Users in rooms
6. Show users in room
7. insert video link
8. Extract video element from link
9. Place it inside the video player
10. Sync the video
11. Chats


